interface IProduct {
  productId: string;
  label: string;
  price: number;
  available: number;
}
