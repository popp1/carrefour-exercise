interface IUser {
  id: string;
  lastName: string;
  firstName?: string;
}
