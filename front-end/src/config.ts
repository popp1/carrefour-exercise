export const devMode = process.env.NODE_ENV === "development";

export const routerBaseName = process.env.REACT_APP_ROUTER_BASE_NAME || "";

export const serverUrlBase =
  process.env.REACT_APP_SERVER_URL_BASE || "http://localhost:3002";
