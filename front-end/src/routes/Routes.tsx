import React from "react";
import { Navigate, RouteObject, useRoutes } from "react-router-dom";

import { App } from "../App";
import PageLayout from "./PageLayout";
import Products from "../components/Products";

export const ROUTES = {
  PRODUCT: "/products",
};

const Routes = () => {
  return useRoutes([
    {
      element: <App />,
      children: [
        {
          element: <PageLayout />,
          children: [
            {
              path: ROUTES.PRODUCT,
              element: <Products />,
            },
          ],
        },
      ],
    },
    {
      path: "*",
      element: <Navigate replace to={ROUTES.PRODUCT} />,
    },
  ] as RouteObject[]);
};

export default Routes;
