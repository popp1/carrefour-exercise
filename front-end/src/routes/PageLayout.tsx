import React from "react";
import { Outlet } from "react-router-dom";
import Header from "../components/Header";
import { GridItem } from "@chakra-ui/react";

const PageLayout = () => {
  return (
    <>
      <GridItem
        boxShadow={
          "0px 0px 1px 0px rgba(4, 45, 103, 0.08), 0px 1px 2px 0px rgba(119, 142, 173, 0.15)"
        }
        area={"header"}
        bg={"white"}
      >
        <Header />
      </GridItem>
      <GridItem area={"main"} overflow={"auto"}>
        <Outlet />
      </GridItem>
    </>
  );
};

export default PageLayout;
