import {
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  useDisclosure,
} from "@chakra-ui/react";
import { ChatIcon } from "@chakra-ui/icons";
import { useSelector } from "react-redux";
import { shoppingCart } from "../../redux/reducer/shoppingCart";
import ProductItem from "../Products/ProductItem";

interface IProps {}

const Header: React.FC<IProps> = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const shoppingCartList = useSelector(shoppingCart);

  return (
    <Flex
      {...{
        justifyContent: "flex-end",
        h: "100%",
        alignItems: "flex-end",
        p: "32px",
      }}
    >
      <Button
        colorScheme="blue"
        variant="outline"
        rightIcon={<ChatIcon />}
        onClick={onOpen}
      ></Button>

      <Drawer isOpen={isOpen} placement="right" onClose={onClose} size="md">
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Create your account</DrawerHeader>

          <DrawerBody>
            <Flex
              className="flexProductsWrapper"
              {...{
                w: "100%",
                h: "100%",
                direction: "column",
                gap: "16px",
                overflow: "auto",
                p: "16px",
              }}
            >
              {shoppingCartList.length > 0 &&
                shoppingCartList.map((productItem) => (
                  <ProductItem
                    key={productItem.productId}
                    {...productItem}
                    noButton
                  />
                ))}
            </Flex>
          </DrawerBody>
          <DrawerFooter></DrawerFooter>
        </DrawerContent>
      </Drawer>
    </Flex>
  );
};

export default Header;
