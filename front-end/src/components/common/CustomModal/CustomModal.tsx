import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  ModalProps,
} from "@chakra-ui/react";

interface IProps extends ModalProps {
  title: string;
  children: React.ReactNode | React.ReactNode[];
  onClose: () => void;
  onOk: () => void;
}

const CustomModal: React.FC<IProps> = ({ title, children, onClose, onOk }) => {
  return (
    <Modal blockScrollOnMount={false} isOpen onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{title}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>{children}</ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={onClose}>
            Close
          </Button>
          <Button onClick={() => onOk()} variant="ghost">
            Ajouter
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default CustomModal;
