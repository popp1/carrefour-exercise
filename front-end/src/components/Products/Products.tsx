import { useGetProductsQuery } from "../../redux/api/productAPI";
import { useSelector } from "react-redux";
import { products } from "../../redux/reducer/product";
import { shallowEqual } from "react-redux";
import { Flex, Spinner, Text } from "@chakra-ui/react";
import ProductItem from "./ProductItem";

interface IProps {}

const Products: React.FC<IProps> = () => {
  const { isLoading, isError } = useGetProductsQuery();

  const productList = useSelector(products, shallowEqual);

  return (
    <Flex
      className="flexProductsWrapper"
      {...{
        w: "100%",
        h: "100%",
        wrap: "wrap",
        gap: "16px",
        overflow: "auto",
        p: "16px",
        justifyContent: "space-between",
      }}
    >
      {isLoading && (
        <Spinner
          m={"auto"}
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      )}
      {isError && <Text m={"auto"}>Erreur, reactualiser</Text>}
      {productList.length > 0 &&
        productList.map((productItem) => (
          <ProductItem key={productItem.productId} {...productItem} />
        ))}
    </Flex>
  );
};

export default Products;
