import { AddIcon } from "@chakra-ui/icons";
import {
  Button,
  Flex,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import CustomModal from "../../common/CustomModal";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addNew } from "../../../redux/reducer/shoppingCart";

interface IProps extends IProduct {
  noButton?: boolean;
}

const ProductItem: React.FC<IProps> = ({
  label,
  productId,
  price,
  available,
  noButton,
}) => {
  const dispatch = useDispatch();
  const { onOpen, isOpen, onClose } = useDisclosure();
  const [toAdd, setToAdd] = useState<number>(0);
  return (
    <Flex
      className="flexProductItemWrapper"
      {...{
        w: "250px",
        h: "200px",
        boxShadow:
          "0px 0px 1px 0px rgba(4, 45, 103, 0.08), 0px 1px 2px 0px rgba(119, 142, 173, 0.15)",
        borderRadius: "6px",
        bg: "white",
        direction: "column",
        p: "8px",
        justifyContent: "space-between",
      }}
    >
      <Flex
        direction={"column"}
        gap="8px"
        w={"100%"}
        alignItems={"center"}
        pt="20px"
      >
        <Text
          {...{
            fontSize: "18px",
            fontWeight: "400",
            lineHeight: "21px",
            color: "grey",
          }}
        >
          {label}
        </Text>
        <Flex {...{ justifyContent: "space-between", w: "100%", p: "0 20px" }}>
          <Text
            {...{
              fontSize: "16px",
              fontWeight: "600",
              lineHeight: "21px",
            }}
          >
            Prix :
          </Text>
          <Text
            {...{
              fontSize: "16px",
              fontWeight: "600",
              lineHeight: "21px",
              color: "blue",
            }}
          >
            {`${price} €`}
          </Text>
        </Flex>
        <Flex {...{ justifyContent: "space-between", w: "100%", p: "0 20px" }}>
          <Text
            {...{
              fontSize: "16px",
              fontWeight: "600",
              lineHeight: "21px",
            }}
          >
            {!noButton ? "disponible(s) :" : "quantité :"}
          </Text>
          <Text
            {...{
              fontSize: "16px",
              fontWeight: "600",
              lineHeight: "21px",
              color: available < 10 ? "red" : "green",
            }}
          >
            {`${available}`}
          </Text>
        </Flex>
      </Flex>
      {!noButton && (
        <Button
          {...{
            onClick: () => {
              onOpen();
            },
          }}
          colorScheme="pink"
          variant="solid"
          leftIcon={<AddIcon />}
        >
          Ajouter au panier
        </Button>
      )}
      {isOpen && (
        <CustomModal
          {...{
            title: "Ajout au panier",
            onClose,
            isOpen,
            onOk: () => {
              dispatch(addNew({ label, productId, price, available: toAdd }));
              onClose();
            },
          }}
        >
          <Flex {...{ w: "100%", h: "100%", gap: "8px", alignItems: "center" }}>
            <Text
              {...{
                fontSize: "18px",
                fontWeight: "400",
                lineHeight: "21px",
                color: "grey",
              }}
            >
              {label}
            </Text>
            <NumberInput
              value={toAdd}
              min={0}
              max={available}
              onChange={(_, val) => {
                setToAdd(val);
              }}
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </Flex>
        </CustomModal>
      )}
    </Flex>
  );
};

export default ProductItem;
