import * as React from "react";
import { ChakraProvider, Grid, theme } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./redux/store";

export const App = () => (
  <ChakraProvider theme={theme}>
    <Provider store={store}>
      <Grid
        className="GridAppContainer"
        h="100vh"
        w="100vw"
        overflow={"hidden"}
        templateAreas={`"header header"
                  "main main"
                  "main main"
                  "main main"
                  "main main"`}
        gridTemplateRows={"10% auto auto auto auto"}
        gridTemplateColumns={"auto"}
        bg="linear-gradient(90deg, #EDEFF3 0%, #E7EAEF 100%)"
      >
        <Outlet />
      </Grid>
    </Provider>
  </ChakraProvider>
);
