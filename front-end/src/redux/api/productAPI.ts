import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { serverUrlBase } from "../../config";

export const productAPI = createApi({
  reducerPath: "productAPI",
  baseQuery: fetchBaseQuery({
    baseUrl: serverUrlBase,
  }),
  endpoints: (builder) => ({
    getProducts: builder.query<IProduct[], void>({
      query: () => `products`,
    }),
    getProduct: builder.query<IUser, string>({
      query: (id) => `products/${id}`,
    }),
    keepProduct: builder.mutation<void, { number: number; id: string }>({
      query: ({ number, id }) => ({
        url: `products/keep`,
        method: "POST",
        data: { number, id },
      }),
    }),
    releaseProduct: builder.mutation<void, { number: number; id: string }>({
      query: ({ id }) => ({
        url: `products/${id}/release`,
        method: "DELETE",
      }),
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useGetProductQuery,
  useGetProductsQuery,
  useLazyGetProductQuery,
  useLazyGetProductsQuery,
} = productAPI;

export default productAPI;
