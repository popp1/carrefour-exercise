import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TRootState } from "../store";

// Define the initial state using that type
const initialState: IProduct[] = [];

export const shoppingCartSlice = createSlice({
  name: "shoppingCart",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    addNew: (state, inputPayload) => [...state, inputPayload.payload],
    release: (state, inputPayload) => [
      ...state.filter(
        (productItem) => productItem.productId === inputPayload.payload
      ),
    ],
  },
});

export const { addNew, release } = shoppingCartSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const shoppingCart = (state: TRootState) => state.shoppingCart;

export default shoppingCartSlice.reducer;
