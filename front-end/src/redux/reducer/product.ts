import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { TRootState } from "../store";
import productAPI from "../api/productAPI";

// Define the initial state using that type
const initialState: IProduct[] = [];

export const productSlice = createSlice({
  name: "products",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addMatcher(
      productAPI.endpoints.getProducts.matchFulfilled,
      (_, action) => {
        return action.payload;
      }
    );
  },
});

// Other code such as selectors can use the imported `RootState` type
export const products = (state: TRootState) => state.product;

export default productSlice.reducer;
