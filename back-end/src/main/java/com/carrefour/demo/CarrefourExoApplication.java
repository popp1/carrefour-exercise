package com.carrefour.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarrefourExoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarrefourExoApplication.class, args);
	}

}
