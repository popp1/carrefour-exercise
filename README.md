# carrefour-exercise

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Launch the React app (front-end)

- Firstly, we need to install Mockoon, we can get the installer from the offcial site https://mockoon.com/

- Then copy the content of the file mockoon.json, localized in the project root directory, to the clipboard :

- start Mockoon

- in the File menu of the app, click on the "Newenvironment rom clipboard", then follow the instructions

- When the environment E shop is setted, start the server

- to start the react app, move to the "front-end" directory, then choose one of the two following commands :
  - yarn install && yarn start
  - npm install && npm start

## for the spring boot project (back-end)

- this app is not yet ready
